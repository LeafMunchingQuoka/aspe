import numpy as np
from Macierz_konduktancji import macierz
from Zrodlo_pradowe import zrodlo_pradowe
from Zrodlo_napieciowe import zrodlo_napieciowe
from Dioda import dioda

# Tworzenie elementow ukladu
# macierz i zrodla:
mg1 = macierz(2)
vs1 = zrodlo_napieciowe(1, 0, 0.5)
d1 = dioda(1, 0)

# przybliżenie poczatkowe
vn_old = np.zeros((mg1.max_size(), 1))

for i in range(40):
    print("####################################################")
    print("Iteracja nr {}\n".format(i+1))

# wyliczenie G i I elementow nieliniowych na podstawie vn_new
    d1.update(vn_old)

# wstawianie szablonow elementow do macierzy konduktancji
    mg1.reset()
    d1.stamp(mg1)
    vs1.stamp(mg1)

# usuniecie wezla masy i stworzenie macierzy do ukl rownan
    a = mg1.A_bez_masy()
    b = mg1.B_bez_masy()
    # wypisanie zawartosci macierzy A i B
    mg1.hello()

# rozwiazanie ukladu rownan
    vn_new = np.linalg.solve(a, b)
    print("Rozwiązanie=\n{}\n".format(vn_new))

# sprawdzenie zbieznosci
    eps = abs(vn_new - vn_old)
    print("Eps={}\n".format(eps))

    vn_old = vn_new
