import math


class dioda:

    alfa = 1/25e-3
    isat = 1e-12
    vmax = 0.8
    vmin = -2
    gmax = alfa*isat*(math.exp(alfa*vmax))
    gmin = alfa*isat*(math.exp(alfa*vmin))
    imax = isat*(math.exp(alfa*vmax) - 1)
    imin = isat*(math.exp(alfa*vmin) - 1)
    bmax = imax - gmax*vmax
    bmin = imin - gmin*vmin

    def __init__(self, anode, knode):

        self.anode = anode
        self.knode = knode
        self.G = 0
        self.Ieq = 0

    def check(self):
        print("Ieq = {}\n".format(self.Ieq))
        print("G = {}\n".format(self.G))
        #print("Imax = {}\n".format(self.imax))
        #print("Bmax = {}\n".format(self.bmax))

    # odpowiedz na sterowanie
    def Id(self, Ud):
        a = self.alfa
        i = self.isat
        return i*(math.exp(a*Ud) - 1)

    # pochodna odpowiedzi na sterowanie
    def Gd(self, Ud):
        a = self.alfa
        i = self.isat
        return a*i*(math.exp(a*Ud))

    # funkcja linearyzująca
    def update(self, vn):
        # wyciagniecie napiecia diody z wektora rozwiazan z poprzedniej iteracji
        aindex = self.anode
        kindex = self.knode

        if aindex == 0:
            va = 0
        else:
            va = vn[aindex-1]

        if kindex == 0:
            vk = 0
        else:
            vk = vn[kindex-1]

        Ud = va - vk
        print("Napięcie na diodzie = {}\n".format(Ud))

        # zaktualizowanie parametrow diody
        if Ud >= self.vmax:
            self.G = self.gmax
            self.Ieq = self.bmax
        elif Ud <= self.vmin:
            self.G = self.gmin
            self.Ieq = self.bmin
        else:
            self.G = self.Gd(Ud)
            self.Ieq = self.Id(Ud)-self.Gd(Ud)*Ud

        self.check()

    # funkcja wstawiająca wartosci modelu liniowego do macierzy konduktancji
    def stamp(self, mg):
        a = self.anode
        k = self.knode
        G = self.G
        Ieq = self.Ieq

        max_node = max(a, k)
        array_size = mg.max_size()  # maksymalny rozmiar macierzy konduktancji

        # zwiekszenie rozmiarow macierzy konduktancji jesli za mala
        if max_node >= array_size:
            diff = max_node - array_size
            mg.pad(diff+1)

        mg.Aarray[a, a] += G
        mg.Aarray[a, k] += -G
        mg.Aarray[k, a] += -G
        mg.Aarray[k, k] += G

        mg.Barray[a] += -Ieq
        mg.Barray[k] += Ieq
