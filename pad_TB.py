import numpy as np
from Rezystor import rezystor
from Macierz_konduktancji import macierz

# Tworzenie elementow ukladu
# macierz i zrodla:
mg1 = macierz(2)
res1 = rezystor(9, 0, 1)
res2 = rezystor(10, 0, 1)

# wstawianie szablonow elementow do macierzy konduktancji
mg1.reset()
res1.stamp(mg1)
res2.stamp(mg1)
print("Rozmiar macierzy = {}".format(mg1.max_size()))
print("Uzycie stamp bezposrednio.\n")
print("Macierz A=\n{}\n".format(mg1.Aarray))
print("Macierz B=\n{}\n".format(mg1.Barray))

mg1.reset()
print("Zerowanie macierzy,\n")
print("Rozmiar macierzy = {}".format(mg1.max_size()))
print("Macierz A=\n{}\n".format(mg1.Aarray))
print("Macierz B=\n{}\n".format(mg1.Barray))


def stamp(mg, res):
    res.stamp(mg)
    res.stamp(mg)


print("Uzycie stamp w funkcji.\n")
stamp(mg1, res1)
stamp(mg1, res2)
print("Rozmiar macierzy = {}".format(mg1.max_size()))
print("Macierz A=\n{}\n".format(mg1.Aarray))
print("Macierz B=\n{}\n".format(mg1.Barray))
