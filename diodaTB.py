import numpy as np
from Rezystor import rezystor
from Macierz_konduktancji import macierz
from Zrodlo_napieciowe import zrodlo_napieciowe
from Zrodlo_pradowe import zrodlo_pradowe
from Dioda import dioda


# Tworzenie elementow ukladu
# macierz i zrodla:
mg1 = macierz(2)
vs1 = zrodlo_napieciowe(pnode=1, nnode=0, value=0.5)
vs2 = zrodlo_napieciowe(pnode=1, nnode=0, value=0.5)
cs1 = zrodlo_pradowe(pnode=0, nnode=1, current=1e-3)
res1 = rezystor(pnode=2, nnode=1, value=1e3)
d1 = dioda(anode=1, knode=0)
d2 = dioda(anode=0, knode=1)
vn = [0, 0, 0, 0]
vn_new = vn

TB = input("Wybierz TB nr: ")
TB = int(TB)

if TB == 1:
    vn = [0, 0, 0, 0]
    for i in range(10):
        print("Iteracja nr {}:\n".format(i+1))
        print("########################################\n")
        mg1.reset()
        d1.update(vn)
        d1.stamp(mg1)
        vs1.stamp(mg1)

        a = mg1.A_bez_masy()
        b = mg1.B_bez_masy()

        print("A:\n{}\n".format(a))
        print("B:\n{}\n".format(b))

        vn_new = np.linalg.solve(a, b)

        for j in range(len(vn_new)):
            print("V{} = {}\n".format(j+1, vn_new[j]))

        eps = abs(vn-vn_new)
        print("Eps = {}\n".format(eps))
        vn = vn_new

elif TB == 2:
    vn = [0, 0, 0, 0]
    for i in range(10):
        print("Iteracja nr {}:\n".format(i+1))
        print("########################################\n")
        mg1.reset()
        d2.update(vn)
        d2.stamp(mg1)
        vs1.stamp(mg1)

        a = mg1.A_bez_masy()
        b = mg1.B_bez_masy()

        print("A:\n{}\n".format(a))
        print("B:\n{}\n".format(b))

        vn_new = np.linalg.solve(a, b)

        for j in range(len(vn_new)):
            print("V{} = {}\n".format(j+1, vn_new[j]))

        eps = abs(vn-vn_new)
        print("Eps = {}\n".format(eps))
        vn = vn_new

elif TB == 3:
    vn = [0, 0, 0, 0]
    for i in range(20):
        print("Iteracja nr {}:\n".format(i+1))
        print("########################################\n")
        mg1.reset()
        d1.update(vn)
        d1.stamp(mg1)
        cs1.stamp(mg1)

        a = mg1.A_bez_masy()
        b = mg1.B_bez_masy()

        print("A:\n{}\n".format(a))
        print("B:\n{}\n".format(b))

        vn_new = np.linalg.solve(a, b)

        for j in range(len(vn_new)):
            print("V{} = {}\n".format(j+1, vn_new[j]))

        eps = abs(vn-vn_new)
        print("Eps = {}\n".format(eps))
        vn = vn_new

elif TB == 4:
    vn = [0, 0, 0, 0]
    for i in range(10):
        print("Iteracja nr {}:\n".format(i+1))
        print("########################################\n")
        mg1.reset()
        d2.update(vn)
        d2.stamp(mg1)
        cs1.stamp(mg1)

        a = mg1.A_bez_masy()
        b = mg1.B_bez_masy()

        print("A:\n{}\n".format(a))
        print("B:\n{}\n".format(b))

        vn_new = np.linalg.solve(a, b)

        for j in range(len(vn_new)):
            print("V{} = {}\n".format(j+1, vn_new[j]))

        eps = abs(vn-vn_new)
        print("Eps = {}\n".format(eps))
        vn = vn_new
