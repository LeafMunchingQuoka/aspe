import numpy as np


class macierz:

    def __init__(self, size):
        self.Aarray = np.zeros((size, size))
        self.Barray = np.zeros((size, 1))
        self.init_size = size

    def pad(self, size):
        # powiekszenie macierzy konduktancji o rozmiar size wzdluz kazdej z osi
        #print("Zmiana rozmiarow macierzy.\n")
        self.Aarray = np.pad(self.Aarray, ((0,size),(0,size)), 'constant', constant_values=0)
        self.Barray = np.pad(self.Barray, ((0,size),(0,0)), 'constant', constant_values=0)

    def hello(self):
        # print("Hello! Im a transconductance matrix!\n")
        print("Lewa strona równania:\n{}\n".format(self.Aarray))
        print("Prawa strona równania:\n{}\n".format(self.Barray))

    def max_size(self):
        return max(self.Aarray.shape)

    def reset(self):
        size = self.init_size
        self.Aarray = np.zeros((size, size))
        self.Barray = np.zeros((size, 1))

    def A_bez_masy(self):
        a = np.delete(self.Aarray, 0, 0)
        a = np.delete(a, 0, 1)
        return a

    def B_bez_masy(self):
        return np.delete(self.Barray, 0, 0)
