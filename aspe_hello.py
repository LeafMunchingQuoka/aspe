import numpy as np
from Rezystor import rezystor
from Macierz_konduktancji import macierz
from Zrodlo_pradowe import zrodlo_pradowe
from Zrodlo_napieciowe import zrodlo_napieciowe
from BJT import bjt

# parametry:
beta = 100
alphaF = beta/(1+beta)
alphaR = alphaF

# Tworzenie elementow ukladu
# macierz i zrodla:
mg1 = macierz(2)
vcc = zrodlo_napieciowe(2, 0, 5)
cs1 = zrodlo_pradowe(0, 1, 10e-6)

# tranzystor:
q1 = bjt(0, 1, 2, alphaF)

# przybliżenie poczatkowe
vn_old = np.ones((mg1.max_size()+1, 1))

print("Vn_init = {} \n".format(vn_old))

# analiza OP:
for i in range(4):
    print("####################################################")
    print("Iteracja nr {}\n".format(i+1))

# linearyzacja modeli elementow nieliniowy (wyliczenie G i I na podstawie vn_new)
    q1.update(vn_old)

# wstawianie szablonow elementow do macierzy konduktancji
    mg1.reset()
    vcc.stamp(mg1)
    cs1.stamp(mg1)
    q1.stamp(mg1)

# usuniecie wezla masy i stworzenie macierzy do ukl rownan
    a = mg1.A_bez_masy()
    b = mg1.B_bez_masy()
    # wypisanie zawartości macierzy
    mg1.hello()

# rozwiazanie ukladu rownan
    vn_new = np.linalg.solve(a, b)
    print("Rozwiązanie=\n{}\n".format(vn_new))

# sprawdzenie zbieznosci
    if i >= 1:
        eps = abs(vn_new - vn_old)
        print("Eps={}\n".format(eps))

# zamiana vn_old przed kolejna iteracja
    vn_old = vn_new
