class rezystor:

    def __init__(self, pnode, nnode, value):
        self.value = value
        self.pnode = pnode
        self.nnode = nnode

    def stamp(self, mg):
        # metoda do wstawienia szablonu elementu do macierzy konduktancji

        n = self.nnode
        p = self.pnode
        value = 1/self.value
        max_node = max(n, p)
        array_size = mg.max_size()  # maksymalny rozmiar macierzy konduktancji

        # zwiekszenie rozmiarow macierzy konduktancji
        if max_node >= array_size:
            diff = max_node - array_size
            mg.pad(diff+1)

        # wpisanie wartosci rezystora do macierzy
        mg.Aarray[n, n] += value
        mg.Aarray[p, p] += value
        mg.Aarray[n, p] += -value
        mg.Aarray[p, n] += -value
