from Dioda import dioda


class bjt:
    def __init__(self, emiter, baza, kolektor, alpha):
        self.emiter = emiter
        self.baza = baza
        self.kolektor = kolektor
        self.dc = dioda(baza, kolektor)
        self.de = dioda(baza, emiter)
        self.alpha = alpha

    def update(self, vn):
        print("Dioda kolektorowa:\n")
        self.dc.update(vn)
        print("Dioda emiterowa:\n")
        self.de.update(vn)

    def stamp(self, mg):

        e = self.emiter
        b = self.baza
        c = self.kolektor
        alpha = self.alpha
        Gde = self.de.G
        Ide = self.de.Ieq
        Gdc = self.dc.G
        Idc = self.dc.Ieq
        # Obliczenie wartości prądów wypływających z wezlow tranzystora
        Ic = alpha*Ide-Idc
        Ie = alpha*Idc-Ide
        Ib = -Ic-Ie
        print("Ic = {}; Ie = {}; Ib = {}\n".format(Ic, Ie, Ib))

        # wstawienie przewodnosci i pradow diod do macierzy
        self.dc.stamp(mg)
        self.de.stamp(mg)

        # odjecie prądow zrodel sterowanych od prawej strony
        mg.Barray[e] += -alpha*self.dc.Ieq
        mg.Barray[c] += -alpha*self.de.Ieq
        #mg.Barray[b] = -mg.Barray[e] - mg.Barray[c]  # <-- cos jest chyba nie tak z tym prądem
