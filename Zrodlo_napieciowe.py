class zrodlo_napieciowe:

    def __init__(self, pnode, nnode, value):

        self.pnode = pnode
        self.nnode = nnode
        self.value = value

    def stamp(self, mg):

        p = self.pnode
        n = self.nnode

        max_node = max(n, p)
        array_size = mg.max_size()  # maksymalny rozmiar macierzy konduktancji

        # zwiekszenie rozmiarow macierzy konduktancji jesli za mala
        if max_node >= array_size:
            diff = max_node - array_size
            mg.pad(diff+1)

        new_index = mg.max_size()  # indeks nowego wiersza i nowej kolumny

        # sprawdzenie czy indeksy sie zgadzaja:

        # print("Matrix max size is {}\n".format(mg.max_size()))
        # print("New index is {}\n".format(new_index))

        mg.pad(1)  # wstawienie nowego wiersza i kolumny dla rownania zrodla (nowy wiersz z wykladu)

        mg.Barray[new_index] = self.value

        mg.Aarray[new_index, p] = 1
        mg.Aarray[new_index, n] = -1
        mg.Aarray[p, new_index] = 1
        mg.Aarray[n, new_index] = -1
