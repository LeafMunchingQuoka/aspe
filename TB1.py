import numpy as np
from Rezystor import rezystor
from Macierz_konduktancji import macierz
from Zrodlo_napieciowe import zrodlo_napieciowe
from BJT import bjt

beta = 100
alpha = beta/(1+beta)

# Tworzenie elementow ukladu
# macierz i zrodla:
mg1 = macierz(2)
vcc = zrodlo_napieciowe(pnode=2, nnode=0, value=1)
res1 = rezystor(pnode=2, nnode=1, value=1e3)
res2 = rezystor(pnode=1, nnode=0, value=1e3)
q1 = bjt(emiter=0, baza=1, kolektor=2, alpha=alpha)

# wstawienie elementow liniowych do macierzy
res1.stamp(mg1)
res2.stamp(mg1)
vcc.stamp(mg1)

# przybliżenie poczatkowe
vn_old = np.zeros((mg1.max_size()-1, 1))
vn_new = vn_old

# macierz elementow nieliniowych
mg2 = macierz(mg1.max_size())
# wstawienie elementow nieliniowych do macierzy
q1.stamp(mg2)

# rozszerzenie macierzy el. nieliniowych o wiersze i kolumny odpowiadające zrodlom
diff = abs(mg1.max_size() - mg2.max_size())

if diff > 0:
    mg1.pad(diff)
    print("Padding\n")

for i in range(10):
    print("Iteracja nr {}:\n".format(i+1))
    print("########################################\n")
    mg2.reset()
    q1.update(vn_new)
    q1.stamp(mg2)
# sprawdzenie macierzy po wstawieniu BJT
    print("Macierz elementow nieliniowych po wstawieniu BJT.\n")
    mg2.hello()
    print("Macierz elementow liniowych.\n")
    mg1.hello()

    # "sklejenie" macierzy el. lin. i macierzy el. nielin.
    a = mg1.A_bez_masy()+mg2.A_bez_masy()
    #print("Lewa strona równania:\n{}\n".format(a))
    b = mg1.B_bez_masy()+mg2.B_bez_masy()
    #print("Prawa strona równania:\n{}\n".format(b))
    print("A:\n{}\n".format(a))
    print("B:\n{}\n".format(b))

    vn_new = np.linalg.solve(a, b)

    for j in range(len(vn_new)):
        print("V{} = {}\n".format(j+1, vn_new[j]))

    eps = abs(vn_old - vn_new)
    print("Eps = {}\n".format(eps))
    vn_old = vn_new
