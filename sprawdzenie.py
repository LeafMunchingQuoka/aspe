from Rezystor import rezystor
from Macierz_konduktancji import macierz
from Zrodlo_pradowe import zrodlo_pradowe
from Zrodlo_napieciowe import zrodlo_napieciowe
from Dioda import dioda
from BJT import bjt

# Tworzenie elementow ukladu
mg1 = macierz(2)
res1 = rezystor(1, 0, 1e3)
cs1 = zrodlo_pradowe(0, 1, 1e-3)
vs1 = zrodlo_napieciowe(1, 0, 5)
d1 = dioda(0, 1)
q1 = bjt(0, 1, 2, 0.99)
vn = [1, 0, 1, 1]

print("###############################\n")
inp = input("Zrodlo_pradowe\n")
mg1.reset()
cs1.stamp(mg1)
mg1.hello()

print("###############################\n")
inp = input("Zrodlo_napieciowe\n")
mg1.reset()
vs1.stamp(mg1)
mg1.hello()

print("###############################\n")
inp = input("Rezystor\n")
mg1.reset()
res1.stamp(mg1)
mg1.hello()

print("###############################\n")
inp = input("Dioda\n")
d1.update(vn)
mg1.reset()
d1.stamp(mg1)
mg1.hello()

print("###############################\n")
inp = input("Tranzystor\n")
q1.update(vn)
mg1.reset()
q1.stamp(mg1)
mg1.hello()
