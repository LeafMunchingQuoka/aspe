class cccs:

    def __init__(self, outp, outn, inp, inn, vin, k):
        self.outp = outp
        self.outn = outn
        self.inp = inp
        self.inn = inn
        self.vin = vin
        self.k = k

    def stamp(self, mg):

        inp = self.inp
        inn = self.inn
        outp = self.outp
        outn = self.outn
        k = self.k
        vin = self.vin

        max_node = max(inp, inn, outp, outn)
        array_size = mg.max_size()  # maksymalny rozmiar macierzy konduktancji

        # zwiekszenie rozmiarow macierzy konduktancji jesli za mala
        if max_node >= array_size:
            diff = max_node - array_size
            mg.pad(diff+1)

        new_index = mg.max_size()  # indeks nowego wiersza i nowej kolumny

        mg.pad(1)  # wstawienie nowego wiersza i kolumny dla rownania zrodla (nowy wiersz z wykladu)

        # wstawienie wartosci do macierzy
        mg.Aarray[outp, new_index] += k
        mg.Aarray[outn, new_index] += -k
        mg.Aarray[inp, new_index] += 1
        mg.Aarray[inn, new_index] += -1
        mg.Aarray[new_index, inn] += -1
        mg.Aarray[new_index, inp] += 1

        mg.Barray[new_index] += vin
