import numpy as np
from Macierz_konduktancji import macierz
from Zrodlo_napieciowe import zrodlo_napieciowe
from Dioda import dioda

# Tworzenie elementow ukladu
# macierz i zrodla:
mg1 = macierz(2)
vs1 = zrodlo_napieciowe(1, 0, 0.5)
d1 = dioda(0, 1)

# przybliżenie poczatkowe
vn_old = np.ones((mg1.max_size(), 1))
print("Vn_old = {}\n".format(vn_old))

for i in range(4):
    print("####################################################")
    print("Iteracja nr {}\n".format(i+1))

# linearyzacja modeli elementow nieliniowy (wyliczenie G i I na podstawie vn_new)
    d1.update(vn_old)

# wstawianie szablonow elementow do macierzy konduktancji
    mg1.reset()
    d1.stamp(mg1)
    print("Wstawienie rezystora\n")
    print("Macierz A=\n{}\n".format(mg1.Aarray))
    print("Macierz B=\n{}\n".format(mg1.Barray))
    vs1.stamp(mg1)
    print("Wstawienie zrodla\n")
    print("Macierz A=\n{}\n".format(mg1.Aarray))
    print("Macierz B=\n{}\n".format(mg1.Barray))

# usuniecie wezla masy i stworzenie macierzy do ukl rownan
    a = mg1.A_bez_masy()
    b = mg1.B_bez_masy()
    print("Macierz A=\n{}\n".format(mg1.Aarray))
    print("Macierz B=\n{}\n".format(mg1.Barray))

# rozwiazanie ukladu rownan
    vn_new = np.linalg.solve(a, b)
    print("Rozwiązanie=\n{}\n".format(vn_new))

# sprawdzenie zbieznosci
    eps = abs(vn_new - vn_old)
    print("Eps={}\n".format(eps))

    vn_old = vn_new
