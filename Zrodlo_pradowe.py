class zrodlo_pradowe:

    def __init__(self, pnode, nnode, current):
        self.pnode = pnode
        self.nnode = nnode
        self.current = current

    def stamp(self, mg):
        # metoda do wstawienia szablonu elementu do macierzy konduktancji

        n = self.nnode
        p = self.pnode
        current = self.current
        max_node = max(n, p)
        array_size = mg.max_size()  # maksymalny rozmiar macierzy konduktancji

        # zwiekszenie rozmiarow macierzy konduktancji
        if max_node >= array_size:
            diff = max_node - array_size
            mg.pad(diff+1)

        # wpisanie wartosci rezystora do macierzy
        mg.Barray[n, 0] += current
        mg.Barray[p, 0] += -current
